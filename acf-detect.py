#!/usr/bin/python2
# coding: utf8

from argparse import ArgumentParser
from os.path import isdir, isfile, join, exists, abspath
from os import access, R_OK, W_OK, X_OK, makedirs
from subprocess import Popen, PIPE, STDOUT
from sys import stdout
from signal import SIGKILL
from prctl import set_pdeathsig
from datetime import datetime
from re import compile as regex_compile
from re import findall as regex_findall

parser = ArgumentParser(description = 'Run ACF detector on directory with images')
add_arg = parser.add_argument
add_arg('--toolbox-dir', type = str, required = True, \
        help = 'path to Dollar\'s toolbox root')
add_arg('--images-dir', type = str, required = True, \
        help = 'path to directory with images')
add_arg('--detector-model', type = str, required = True, \
        help = 'path to detector model binary')
add_arg('--workdir', type = str, required = True, \
        help = 'path to directory where work files are stored')
add_arg('--base-label', type = str, required = True, \
        help = 'name of image set being used')
arg = parser.parse_args()

# sorting arguments and checking arguments
is_accessable_dir = lambda name: isdir(name) and access(name, R_OK | X_OK)
is_readable_file = lambda name: isfile(name) and access(name, R_OK)

toolbox = abspath(arg.toolbox_dir)
if not is_accessable_dir(toolbox):
    print 'toolbox directory accessing failure'
    exit(1)
version_path = join(toolbox, 'VERSION')
url_path = join(toolbox, 'URL')
if not is_readable_file(version_path):
    print 'version file is not found in toolbox directory'
    exit(1)
version = open(version_path).read().strip()
if not is_readable_file(url_path):
    print 'url file in not found in toolbox directory'
    exit(1)
url = open(url_path).read().strip()

label = arg.base_label
images_dir = abspath(arg.images_dir)
if not is_accessable_dir(images_dir):
    print 'input image directory accessing failure'
    exit(1)

workdir = abspath(arg.workdir)
if not is_accessable_dir(workdir):
    makedirs(workdir)

detector_file = abspath(arg.detector_model)
if not is_readable_file(detector_file):
    print 'detector file accessing failure'
    exit(1)

detections_file = join(workdir, 'detections.txt')

# matlab script template here
matlab_code = """\
addpath(genpath('{toolbox}'));
load('{detector_file}');
images = dir('{images_dir}/*');
images = {{images.name}};
images = images(cell2mat(cellfun(@(x){{x(1)~='.'}}, images)));
count = size(images, 2); tic;
dets = cell(1, count);
matlabpool open;
parfor image_idx = 1:count;
    name = images{{image_idx}};
    path = ['{images_dir}/' name];
    dets{{image_idx}} = acfDetect(imread(path), detector);
end
time = toc();
outfd = fopen('{detections_file}', 'w');
for image_idx = 1:count;
    curr_dets = dets{{image_idx}};
    for idx = 1:size(curr_dets, 1),
        det = int32(curr_dets(idx, 1:4)); conf = curr_dets(idx, 5);
        fprintf(outfd, '%s;%d;%d;%d;%d;%f\\n', images{{image_idx}}, det, conf);
    end
end
fclose(outfd);
fprintf(1, '# elapsed time: %ds\\n', int32(time));
exit(0);
"""
# instantiation of matlab code template
matlab_code = matlab_code.format(**locals())
matlab_func = 'exec_detect'
matlab_file = '%s.m' % matlab_func
open(join(workdir, matlab_file), 'w').write(matlab_code)

# preparing log file
matlab_log_filename = join(workdir, 'matlab.log')
log_file = open(matlab_log_filename, 'w')
log_write = log_file.write

# preparing matlab command
command = ['matlab', '-nodesktop', '-nodisplay', '-r', matlab_func]
proc = Popen(command, cwd = workdir,
             stdout = PIPE,
             stderr = STDOUT,
             close_fds = True,
             preexec_fn = lambda: set_pdeathsig(SIGKILL))
for line in iter(proc.stdout.readline, ''):
    stdout.write(line)
    log_write(line)
retcode = proc.wait()
log_file.close()

# writing commentaries about the detector
comment_base = """\
the report is built at: {current_time}
Dollar toolbox version used: {version}
input images base label: {label}
input images directory: {images_dir}
detector model from file: {detector_file}

matlab return code: {retcode}
time on testing elapsed: {consumed_time}

matlab invokation log:
{matlab_log}
"""
current_time = str(datetime.now())
consumed_time = 'N/A'

log_lines = open(matlab_log_filename, 'r').readlines()[7:]
# searching for time measurement printout
fulltime_regex = regex_compile('^# elapsed time: (.*)$')
target = filter(lambda l: len(l) == 1, map(lambda s: regex_findall(fulltime_regex, s), log_lines))
if len(target) == 1:
    consumed_time = target[0][0]
# preparing full matlab invokation log
matlab_log = ''.join(log_lines)
# writing comments to file
open(join(workdir, 'comment.txt'), 'w').write(comment_base.format(**locals()))

# exiting
if retcode != 0:
    print '# return code indicates error: %d' % retcode
    exit(1)
exit(0)
