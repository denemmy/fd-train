from os.path import abspath, join, exists
from os import makedirs
from subprocess import Popen, PIPE, STDOUT
from sys import stdout

def main():
    workdir = abspath('train-test')
    if exists(workdir):
        print 'workdir is already exists'
        exit(1)
    makedirs(workdir)


    matlab_code = """\
    diary('matlab.log');
    fprintf('wait me\\n');
    pause(3);
    fprintf('thank you\\n');
    diary off;
    exit(1);
    """

    # instantiation of matlab code template
    matlab_func = 'exec_training'
    matlab_file = '%s.m' % matlab_func
    open(join(workdir, matlab_file), 'w').write(matlab_code)

    # preparing matlab command
    command = ['matlab', '-nosplash',  '-wait']
    command += ['-nodesktop', '-nodisplay']
    command += ['-r', matlab_func]

    proc = Popen(command, cwd=workdir,
             stdout=PIPE,
             stderr=PIPE)

    retcode = proc.wait()
    print retcode


if __name__ == '__main__':
    main()