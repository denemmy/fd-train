#!/usr/bin/python2

from PyQt4 import QtCore, QtGui, Qt
from sys import argv, exit
from os.path import join, basename
from xml.dom.minidom import parse
from argparse import ArgumentParser

class BBoxItem(QtGui.QGraphicsItem):
    def __init__(self, w, h, parent, **kwargs):
        super(BBoxItem, self).__init__(parent)
        self.width = w
        self.height = h
        color = {False: QtGui.QColor(255, 0, 0),
                 True:  QtGui.QColor(0, 240, 0),
                }[kwargs.get('gt', False)]
        style = {False: Qt.Qt.SolidLine,
                 True:  Qt.Qt.DotLine,
                }[kwargs.get('ignore', False)]
        pen = QtGui.QPen()
        pen.setColor(color)
        pen.setStyle(style)
        self.pen = pen
        color.setAlpha(30)
        self.brush = QtGui.QBrush(color)
        self.config = kwargs
        self.font = QtGui.QFont('Meslo LG S DZ', 8)
    def boundingRect(self):
        return QtCore.QRectF(0, 0, self.width, self.height)
    def paint(self, painter, *other):
        painter.setPen(self.pen)
        painter.setBrush(self.brush)
        painter.drawRect(0., 0., self.width, self.height)
        level = self.config.get("confidence", None)
        if level != None:
            painter.setPen(QtGui.QPen(QtGui.QColor(240, 240, 0)))
            conf_label = '%.1f' % level
            sizes = QtGui.QFontMetrics(self.font).size(Qt.Qt.TextSingleLine, conf_label)
            painter.setFont(self.font)
            painter.drawText(3, sizes.height(), conf_label)
    def confidenceThreshold(self, threshold):
        level = self.config.get("confidence", None)
        if level != None:
            self.hide() if level < threshold else self.show()
        return self.isVisible()

class ImageView(QtGui.QGraphicsView):
    zoom_in_step = 1.15
    zoom_out_step = 1. / zoom_in_step
    def __init__(self, scene, parent):
        super(ImageView, self).__init__(scene, parent)
        self.viewport = QtCore.QRectF()
        self.setDragMode(QtGui.QGraphicsView.ScrollHandDrag)
        self.setFrameShape(QtGui.QFrame.NoFrame)
        self.setVerticalScrollBarPolicy(Qt.Qt.ScrollBarAlwaysOff)
        self.setHorizontalScrollBarPolicy(Qt.Qt.ScrollBarAlwaysOff)
        self.setRenderHints(Qt.QPainter.Antialiasing | Qt.QPainter.SmoothPixmapTransform)
    def setViewport(self, rect):
        self.viewport = rect
    def resizeEvent(self, event):
        QtGui.QGraphicsView.resizeEvent(self, event)
        self.fitInView(self.viewport, Qt.Qt.KeepAspectRatio)
    def wheelEvent(self, event):
        step = self.zoom_in_step if event.delta() > 0 else self.zoom_out_step
        self.setTransformationAnchor(QtGui.QGraphicsView.AnchorUnderMouse)
        self.scale(step, step)

class BaseStorage:
    def __init__(self, **kwargs):
        self.current_idx = 0
        self.images_dir = kwargs.get('images_dir', '')
        storage_type = kwargs['storage_type']
        # processing detections file
        if storage_type == 'detections':
            detections_file = kwargs['filein']
            storage = {}
            for line in open(detections_file):
                fields = line.rstrip().split(';')
                if len(fields) != 6:
                    raise RuntimeError('detection file format violation')
                name = fields[0]
                config = {'gt': False}
                config['confidence'] = float(fields[5])
                storage_item = storage.get(name, [])
                storage_item.append(tuple(map(int, fields[1:5]) + [config]))
                storage[name] = storage_item
            self.storage = sorted(storage.items(), key = lambda x: x[0])
        elif storage_type == 'gt':
            self.storage = []
            if 'filein' in kwargs:
                markup_file = kwargs['filein']
                root = parse(markup_file).documentElement
                for element in root.getElementsByTagName('Photo'):
                    name = element.getElementsByTagName('File')[0].childNodes[0].data
                    bboxes = element.getElementsByTagName('BBoxes')
                    bboxes_list = []
                    if len(bboxes) > 0:
                        for bbox in bboxes[0].getElementsByTagName('BBox'):
                            config = {'gt': True, 'ignore': False}
                            config.update(bbox.attributes.items())
                            if config.get('Occluded', None) == 'Occluded':
                                config['ignore'] = True
                            x = int(bbox.getElementsByTagName('X')[0].childNodes[0].data)
                            y = int(bbox.getElementsByTagName('Y')[0].childNodes[0].data)
                            w = int(bbox.getElementsByTagName('W')[0].childNodes[0].data)
                            h = int(bbox.getElementsByTagName('H')[0].childNodes[0].data)
                            bboxes_list.append((x, y, w, h, config))
                    self.storage.append((name, bboxes_list))
            else:
                raise RuntimeError('unsupported ground-truth format')
        elif storage_type != 'nomarkup':
            raise RuntimeError('unknown storage type')
    def index(self):
        return self.current_idx
    def get_imagename(self):
        return join(self.images_dir, self.storage[self.current_idx][0])
    def get_markup(self):
        return self.storage[self.current_idx][1]
    def rewind(self, offset):
        idx = self.current_idx + offset
        if idx < 0 or idx >= len(self.storage):
            return False
        self.current_idx = idx
        return True
    def get_progress(self):
        return '%d from %d' % (self.current_idx + 1, len(self.storage))
    def get_size(self):
        return len(self.storage)

class MainWindow(QtGui.QMainWindow):
    def __init__(self, storage, confidence_spinner):
        super(MainWindow, self).__init__()
        self.storage = storage
        self.scene = QtGui.QGraphicsScene(self)
        self.view = ImageView(self.scene, self)
        self.bar = QtGui.QStatusBar(self)
        self.bar.setSizeGripEnabled(False)
        self.setStatusBar(self.bar)
        self.name_label = QtGui.QLabel(self.bar)
        self.progress_label = QtGui.QLabel(self.bar)
        self.bar.addWidget(self.name_label, 4)
        self.bar.addWidget(self.progress_label, 4)
        if confidence_spinner:
            self.progress_label.setAlignment(Qt.Qt.AlignLeft | Qt.Qt.AlignVCenter)
            self.spinner_label = QtGui.QLabel(self.bar)
            self.spinner_label.setText('confidence threshold:')
            self.spinner_label.setAlignment(Qt.Qt.AlignRight | Qt.Qt.AlignVCenter)
            self.spinner = QtGui.QDoubleSpinBox(self.bar)
            self.suppressed_label = QtGui.QLabel(self.bar)
            self.bar.addWidget(self.suppressed_label, 1)
            self.bar.addWidget(self.spinner_label, 2)
            self.bar.addWidget(self.spinner, 1)
            self.spinner.valueChanged.connect(self.applyConfidenceThreshold)
            self.spinner.setFocusPolicy(Qt.Qt.ClickFocus)
            self.spinner.setRange(0, 1e10)
        else:
            self.progress_label.setAlignment(Qt.Qt.AlignRight | Qt.Qt.AlignVCenter)
            self.spinner = None
        self.pixmapitem = QtGui.QGraphicsPixmapItem()
        self.scene.addItem(self.pixmapitem)
        self.pixmapitem.setPos(0., 0.)
        self.shortcut_next = QtGui.QShortcut(QtGui.QKeySequence('Right'), self, self.move_next)
        self.shortcut_prev = QtGui.QShortcut(QtGui.QKeySequence('Left'), self, self.move_prev)
        self.reloadSample()
    def focusView(self):
        self.view.focusWidget()
    def setConfidenceThreshold(self, value):
        if self.spinner:
            self.spinner.setValue(value)
    def reloadSample(self):
        self.clearBBoxes()
        image_name = storage.get_imagename()
        self.loadImage(image_name)
        self.addBBoxes(self.storage.get_markup())
        self.name_label.setText(basename(image_name))
        self.progress_label.setText(storage.get_progress())
    def addBBoxes(self, bboxes):
        for x, y, w, h, params in bboxes:
            bbox = BBoxItem(w, h, self.pixmapitem, **params)
            bbox.setPos(x, y)
        if self.spinner:
            self.applyConfidenceThreshold(self.spinner.value())
    def getBBoxItems(self):
        return filter(lambda item: isinstance(item, BBoxItem), self.scene.items())
    def applyConfidenceThreshold(self, value):
        visibility = map(lambda bbox: bbox.confidenceThreshold(value), self.getBBoxItems())
        suppressed = len(filter(lambda e: e == False, visibility)) # hidden = low confidence
        if suppressed == 0:
            message = '%d detection[s]' % len(visibility)
        else:
            message = '%d (+%d suppressed) detection[s]' % (len(visibility) - suppressed, suppressed)
        self.suppressed_label.setText(message)
    def clearBBoxes(self):
        map(lambda bbox: self.scene.removeItem(bbox), self.getBBoxItems())
    def loadImage(self, path):
        image = QtGui.QPixmap(path)
        if image.isNull():
            QtGui.QMessageBox.information(self, "Viewer", "Cannot load %s" % path)
        self.pixmapitem.setPixmap(image)
        self.scene.setSceneRect(self.pixmapitem.boundingRect())
        self.view.setViewport(self.pixmapitem.boundingRect())
        self.view.fitInView(self.pixmapitem.boundingRect(), Qt.Qt.KeepAspectRatio)
        self.scene.update()
    def resizeEvent(self, event):
        size = event.size()
        width, height = size.width(), size.height()
        self.view.setGeometry(0, 0, width, height - self.bar.height())
        super(MainWindow, self).resizeEvent(event)
    def move_next(self):
        if self.storage.rewind(1):
            self.reloadSample()
    def move_prev(self):
        if self.storage.rewind(-1):
            self.reloadSample()

if __name__ == '__main__':
    parser = ArgumentParser(description = 'Viewer for ground-truth data or detections list')
    add_arg = parser.add_argument
    add_arg('--images-dir', type = str, required = True, \
            help = 'path to directory with images (will be joined with sample-name pathes')
    add_arg('--detections-file', type = str, default = None, \
            help = 'text file with detections: "image-file;x;y;w;h;confidence"')
    add_arg('--batch-file', type = str, default = None, \
            help = 'xml-file with ground-truth data (batch labeller)')
    arg = parser.parse_args()
    confidence_spinner = False
    if arg.detections_file != None and arg.batch_file != None:
        print 'detection file and batch file options are mutually exclusive'
        exit(1)
    if arg.detections_file != None:
        storage = BaseStorage(storage_type = 'detections',
                              filein = arg.detections_file,
                              images_dir = arg.images_dir)
        confidence_spinner = True
    if arg.batch_file != None:
        storage = BaseStorage(storage_type = 'gt',
                              filein = arg.batch_file,
                              images_dir = arg.images_dir)
    if storage.get_size() == 0:
        print 'empty storage is provided, there is nothing to display'
        exit(0)
    app = QtGui.QApplication(argv)
    window = MainWindow(storage, confidence_spinner)
    window.show()
    exit(app.exec_())
