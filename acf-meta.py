#!/usr/bin/python2
# coding: utf8

from argparse import ArgumentParser, RawTextHelpFormatter
from os.path import isdir, isfile, join, exists
from os.path import abspath, basename, splitext, expanduser, dirname
from os import access, R_OK, W_OK, X_OK, makedirs
from subprocess import Popen, PIPE, STDOUT
from datetime import datetime

desc_epilog = """\
This script is designed to TRAIN acf detector on given base and then TEST it
over another given base. It is suggested that in one run of this script only
one new detector is built then tested over one or more bases.
Working tree organized like: {workdir}/{train-label} per trained model and
{train-label}/evals/{test-base-name} per testing base for trained model.
If directory for train-label is already exist then it won't be retrained.

To make base handling more accurate and easy each base (train/test) must
contain file with name 'label' along with 'gt' and 'imgs' subfolders with
base name to display on plots.

For easy interaction the script can post results via email if is specified
in parameters. To make this available SMTP configuration is required on path
~/.acf-meta.conf which should look like:
    email-username = NAME
    email-password = PASS
    email-server = SERVER
    email-ssl = on|off
"""

parser = ArgumentParser(description = 'Train and test acf detector', \
                        epilog = desc_epilog, formatter_class = RawTextHelpFormatter)
add_arg = parser.add_argument
add_arg('--toolbox-dir', type = str, required = True, \
        help = 'path to Dollar\'s toolbox root')
add_arg('--input-base', type = str, required = True, \
        help = 'path to directory with images and gt to learn detector from')
add_arg('--test-bases', type = str, nargs = '+', required = True, \
        help = 'pathes to directories with images and gt to evaluate detector')
add_arg('--workdir', type = str, required = True, \
        help = 'path to working directory')
add_arg('--mail-to', type = str, nargs = '+', default = [], \
        help = 'send emails with results to given addresses')
add_arg('trainopts', metavar='opts', type = str, nargs='*', \
        help='options for acf-train script')
arg = parser.parse_args()

# sorting arguments and checking arguments
is_accessable_dir = lambda name: isdir(name) and access(name, R_OK | X_OK)
is_readable_file = lambda name: isfile(name) and access(name, R_OK)

toolbox = abspath(arg.toolbox_dir)
if not is_accessable_dir(toolbox):
    print 'toolbox directory accessing failure'
    exit(1)

train_dir = abspath(arg.input_base)
train_img = join(train_dir, 'imgs')
train_gt = join(train_dir, 'gt')
if not all(map(is_accessable_dir, [train_img, train_gt])):
    print 'input base directories accessing failure'
    exit(1)
train_label = join(train_dir, 'label')
if not is_readable_file(train_label):
    print 'input base label file accessing failure'
    exit(1)
train_label = open(train_label, 'r').read().strip()

test_dirs = map(abspath, arg.test_bases)
test_imgs = map(lambda s: join(s, 'imgs'), test_dirs)
test_gts = map(lambda s: join(s, 'gt'), test_dirs)
if not all(map(is_accessable_dir, test_imgs + test_gts)):
    print 'testing bases directories accessing failure'
    exit(1)
test_labels = map(lambda s: join(s, 'label'), test_dirs)
if not all(map(is_readable_file, test_labels)):
    print 'testing bases label files accessing failure'
    exit(1)
test_labels = map(lambda f: open(f, 'r').read().strip(), test_labels)
test_list = zip(test_dirs, test_imgs, test_gts, test_labels)
del test_dirs, test_imgs, test_gts, test_labels

mail_targets = arg.mail_to
if len(mail_targets) == 0:
    def send(*args, **kargs):
        pass
else:
    config_file = abspath(expanduser('~/.acf-meta.conf'))
    if not is_readable_file(config_file):
        print '~/.acf-meta.conf accessing failure'
        exit(1)
    config = open(config_file).readlines()
    config = filter(lambda x: x != '', map(lambda s: s.split('#')[0].strip(), config))
    config = map(lambda x: map(str.strip, x.split('=')), config)
    if not all(map(lambda x: len(x) == 2, config)):
        print 'configuration file syntax violated'
        exit(1)
    config = dict(config)
    username = config.get('email-username', None)
    if not username:
        print 'email-username is not set'
        exit(1)
    password = config.get('email-password', None)
    if not password:
        print 'email-password is not set'
        exit(1)
    server = config.get('email-server', None)
    if not server:
        print 'email-server is not set'
        exit(1)
    ssl_enabled = config.get('email-ssl', 'off')
    if ssl_enabled not in {'off', 'on'}:
        print 'bad value of email-ssl'
        exit(1)
    ssl_enabled = ssl_enabled == 'on'

    # import the email modules we'll need
    from email.mime.text import MIMEText
    from email.parser import Parser
    from email.utils import formataddr
    from email.header import decode_header
    from email.mime.multipart import MIMEMultipart
    from email.mime.base import MIMEBase
    from email.Utils import COMMASPACE, formatdate
    from email import Encoders
    # import library for outgoing mail processing
    import smtplib
    def send(**kargs):
        # making MIME email
        filenames = kargs.get('filenames', [])
        text_part = MIMEText(kargs['body'], 'plain', 'UTF-8')
        if filenames == []:
            out = text_part
        else:
            out = MIMEMultipart()
            out.attach(text_part)
        # attaching files
        name_mapping = kargs.get('mapper', dict())
        for fname in filenames:
            if not access(fname, R_OK):
                continue
            part = MIMEBase('application', 'octet-stream')
            part.set_payload(open(fname, 'rb').read())
            Encoders.encode_base64(part)
            name = name_mapping.get(fname, basename(fname)) # fallback to basename of path
            part.add_header('Content-Disposition', 'attachment; filename="%s"' % name)
            out.attach(part)
        # attaching headers
        full_bcc = kargs.get('shadow', False)
        out['Date'] = formatdate(localtime = True)
        out['Subject'] = kargs['subject']
        out['From'] = formataddr((kargs['myname'], username))
        if not full_bcc:
            out['To'] = COMMASPACE.join(mail_targets)
        else:
            out['To'] = 'undisclosed-recipients: ;'
        if 'inreply' in kargs and kargs['inreply'] != None:
            out['In-Reply-To'] = kargs['inreply']
        if 'references' in kargs:
            refs = kargs['references']
            if refs != [] and refs != None:
                out['References'] = ' '.join(kargs['references'])
        try:
            if ssl_enabled:
                connection = smtplib.SMTP_SSL(server)
            else:
                connection = smtplib.SMTP(server)
            connection.login(username, password)
            connection.sendmail(username, mail_targets, out.as_string())
            connection.quit()
            return True
        except BaseException as err:
            print err
            return False

current_report = """\
Hi,
new report is available: generated at {current_time}.
Training base label {train_label}.
"""
trainopts = arg.trainopts
current_time = str(datetime.now())
current_report = current_report.format(**locals())

attachments = []
email_names = dict()
def log(text):
    global current_report
    current_report += text
def attach(filename, email_name = None):
    global attachments, email_names
    if not email_name:
        email_name = basename(filename)
    attachments.append(filename)
    email_names[filename] = email_name
def exit_with_message(subject, retcode):
    global attachments, current_report, email_names
    good = send(filenames = attachments, body = current_report, \
                subject = '[REPORT] ' + subject, \
                myname = 'ACF-bot', \
                mapper = email_names)
    if not good:
        exit(1)
    exit(retcode)

script_dir = dirname(abspath(__file__))
# preparing directories
workdir = arg.workdir
if not exists(workdir):
    makedirs(workdir)
model_dir = join(workdir, train_label)
model_file = join(model_dir, 'Detector.mat')
if exists(model_dir):
    if not exists(model_file):
        print 'model dir "%s" is present, but detector file is not' % model_dir
        exit(1)
    print 'model dir "%s" contains detector, skipping training' % model_dir
    log('ACT-train stage was skipped because model is already there.\n')
else:
    # training stage
    cmd = ['./acf-train.py', '--toolbox-dir', toolbox, '--input-base', train_dir, \
           '--label', train_label, '--workdir', model_dir]
    cmd += trainopts
    retcode = Popen(cmd, cwd = script_dir).wait()
    attach(join(model_dir, 'comment.txt'), 'acf-train.txt')
    if retcode != 0:
        log('\nACT-train stage failed with code: %d.\n' % retcode)
        exit_with_message('acf-train FAILED', 1)
    if not exists(model_file):
        log('\nACT-train stage hasn\'t produced model-file\n')
        exit_with_message('acf-train FAILED', 1)

# detection and evaluation stage
roc_data = [] # list of tuples (roc.txt path, label)
for test_dir, test_img, test_gt, test_label in test_list:
    evals_dir = join(model_dir, 'evals')
    curr_eval_dir = join(evals_dir, test_label)
    if not exists(evals_dir):
        makedirs(evals_dir)
    cmd = ['./acf-detect.py', '--toolbox-dir', toolbox, '--base-label', test_label, \
           '--detector-model', model_file, '--images-dir', test_img, '--workdir', curr_eval_dir]
    retcode = Popen(cmd, cwd = script_dir).wait()
    if retcode != 0:
        log('ACT-detect for label %s failed with code %d\n' % (test_label, retcode))
        continue
    attach(join(curr_eval_dir, 'comment.txt'), 'acf-detect-%s.txt' % test_label)
    # evaluation stage
    cmd = ['./acf-evaluate.py', '--input-base', test_dir, \
           '--detections-list', join(curr_eval_dir, 'detections.txt'), \
           '--workdir', curr_eval_dir, '--flush-barrier', '1.0', \
           '--good-iou', '50.']
    retcode = Popen(cmd, cwd = script_dir).wait()
    if retcode != 0:
        log('ACT-evaluate for label %s failed with code %d\n' % (test_label, retcode))
        continue
    roc_file = join(curr_eval_dir, 'roc.txt')
    if not exists(roc_file):
        log('ACT-evaluate hasn\'t produced roc.txt for label %s\n' % test_label)
        continue
    roc_data.append((roc_file, 'ROC for test base: %s' % test_label))
    log('ACT-detect and evaluate stages for %s: OK.\n' % test_label)

if len(roc_data) == 0:
    log('There is no ROC data available, skipping plot-stage\n')
    exit_with_message('NO ROC available', 1)

# plotting stage
plot_output = join(model_dir, 'joint-roc.png')
roc_pathes, roc_titles = zip(*roc_data)
cmd = ['./roc-multiplot.py', '--output', plot_output, '--minor-grid', \
       '--roc-files'] + list(roc_pathes) + ['--titles'] + list(roc_titles)
retcode = Popen(cmd, cwd = script_dir).wait()
if retcode != 0:
    log('ROC-multiplot failed with code %d\n' % retcode)
else:
    log('ROC-multiplot stage: OK.\n')
    attach(plot_output, 'roc.png')
exit_with_message('Results are available', 0)
