#!/usr/bin/python2
from argparse import ArgumentParser
from numpy import polyfit, polyval, RankWarning
from os.path import isdir, isfile, join, exists, abspath
from os import access, R_OK, W_OK, X_OK, makedirs
from shutil import rmtree
from subprocess import Popen
from tempfile import mkdtemp
from warnings import simplefilter

# suppress rank warning (low values are used)
simplefilter('ignore', RankWarning)

def mean_squared_error(polynom, x, y):
    assert(len(x) == len(y) != 0)
    approx_y = map(lambda point: polyval(polynom, point), x)
    return sum(map(lambda a, b: pow(a - b, 2), approx_y, y)) / len(y)

# name string -> function
fixing_methods_table = dict()

def leave_as_is(*args):
    pass
fixing_methods_table['nofix'] = leave_as_is

def max_fix(x_values, y_values):
    # maximum fixing -- fix curve like:
    #           F              F
    #    * *   *  ==>   * * * *
    #   *   * *        *
    #  S     *        S
    # it's assumed that x_values are sorted by ascending
    curr_idx = 0
    last_idx = len(x_values) - 1
    reached_val = y_values[curr_idx]
    while curr_idx <= last_idx:
        reached_val = max(reached_val, y_values[curr_idx])
        y_values[curr_idx] = reached_val
        curr_idx += 1
fixing_methods_table['max'] = max_fix

def min_fix(x_values, y_values):
    # minimum fixing -- fix curve like:
    #           F              F
    #    * *   *  ==>         *
    #   *   * *              *
    #  S     *        S * * *
    # it's assumed that x_values are sorted by ascending
    curr_idx = len(x_values) - 1
    last_idx = 0
    reached_val = y_values[curr_idx]
    while curr_idx >= last_idx:
        reached_val = min(reached_val, y_values[curr_idx])
        y_values[curr_idx] = reached_val
        curr_idx -= 1
fixing_methods_table['min'] = max_fix

def linear_fix(x_values, y_values):
    # linear fixing -- fix curve like:
    #           F              F
    #    *D*   *  ==>       *
    #   *   * *          *
    #  S     L        S
    # it's assumed that x_values are sorted by ascending
    def linear_fix_round(x_values, y_values):
        last_idx = len(x_values) - 1
        # searching for first descending point (D)
        fixing = False
        curr_idx = 0
        while curr_idx < last_idx:
            next_idx = curr_idx + 1
            if y_values[curr_idx] > y_values[next_idx]:
                fixing = True
                break
            curr_idx += 1
        if not fixing:
            return False
        # fixing y-value of local peak, searching for F and L points
        peak_idx = curr_idx
        peak_val = y_values[peak_idx]
        lower_idx = peak_idx
        lower_val = peak_val
        while curr_idx < last_idx:
            curr_val = y_values[curr_idx]
            if lower_val > curr_val:
                lower_val = curr_val
                lower_idx = curr_idx
            if curr_val > peak_val:
                break
            curr_idx += 1
        last_idx = curr_idx
        # tracing back from D to S
        curr_idx = peak_idx
        while curr_idx > 0:
            if y_values[curr_idx] < lower_val:
                break
            curr_idx -= 1
        begin_idx = curr_idx
        # making line from S to F
        x_0 = x_values[begin_idx]
        x_1 = x_values[last_idx]
        y_0 = y_values[begin_idx]
        y_1 = y_values[last_idx]
        k = (y_1 - y_0) / (x_1 - x_0)
        b = (x_1 * y_0 - x_0 * y_1) / (x_1 - x_0)
        # replacing all involved points
        curr_idx = begin_idx + 1
        while curr_idx < last_idx:
            curr_x = x_values[curr_idx]
            y_values[curr_idx] = k * curr_x + b
            curr_idx += 1
        return True
    next_iteration = True
    while next_iteration:
        next_iteration = linear_fix_round(x_values, y_values)
fixing_methods_table['linear'] = linear_fix

# done with preparing
parser = ArgumentParser(description = 'Converts ACF detector to binary format')
add_arg = parser.add_argument
add_arg('--detector-model', type = str, required = True, \
        help = 'path to detector model mat file')
add_arg('--roc-file', type = str, default = None, \
        help = 'roc file to base mapping on')
add_arg('--no-mapping', action = 'store_true', \
        help = 'do not append confidence-precision mapping')
add_arg('--curve-fixup', type = str, default = 'nofix', \
        help = 'pre-filter for precision(confidence) points: linear, min, max, nofix')
add_arg('--max-mse', type = float, default = 1e-7, \
        help = 'maximum mean squared error for line-piece')
add_arg('--output', type = str, required = True, \
        help = 'output binary model path')
add_arg('--visual-output', type = str, default = None, \
        help = 'file to save output mapping visualization')
arg = parser.parse_args()
# sorting arguments and checking arguments
is_accessable_dir = lambda name: isdir(name) and access(name, R_OK | X_OK)
is_readable_file = lambda name: isfile(name) and access(name, R_OK)

input_file = abspath(arg.detector_model)
if not is_readable_file(input_file):
    print 'input model accessing failure'
    exit(1)
output_file = abspath(arg.output)
if exists(output_file):
    print 'output file already exists, won\'t override'
    exit(1)
roc_file = abspath(arg.roc_file) if arg.roc_file else None
if roc_file and not is_readable_file(roc_file):
    print 'roc file accessing failure'
    exit(1)
no_mapping = arg.no_mapping
if not no_mapping and not roc_file:
    print 'need roc file to calculate mapping from'
    exit(1)
fixup_method = arg.curve_fixup
if fixup_method not in fixing_methods_table:
    print 'don\'t know "%s" fixup method' % fixup_method
    exit(1)
if fixup_method == 'nofix':
    print 'WARNING: unfixed roc-curve may break searching for inverse mapping!'
max_error = arg.max_mse
visualize_file = abspath(arg.visual_output) if arg.visual_output else None
if visualize_file and exists(visualize_file):
    print 'visualization file already exists'
    exit(1)
if visualize_file and no_mapping:
    print 'with no mapping enabled there is nothing to visualize'
    exit(1)

if visualize_file:
    # trying to load required libs for visualization
    from matplotlib import use
    use('AGG')
    from matplotlib.pyplot import figure, subplot, plot, legend, savefig, close, MaxNLocator
    from matplotlib.font_manager import FontProperties
    from numpy import arange

# matlab binary model writer template
matlab_code = """\
load('{input_file}');
fid = fopen('{output_file}', 'w');
d = detector.clf;
len = size(d.weights, 2);
td = d.treeDepth;
kol = 2 ^ (td + 1) - 1;
fwrite(fid, 3, 'int'); %% write version number
fwrite(fid, detector.opts.pPyramid.pChns.shrink, 'int');
fwrite(fid, detector.opts.pPyramid.nPerOct, 'int');
fwrite(fid, detector.opts.pPyramid.nOctUp, 'int');
fwrite(fid, detector.opts.modelDs(1, 1), 'int');
fwrite(fid, detector.opts.modelDs(1, 2), 'int');
fwrite(fid, detector.opts.modelDsPad(1, 1), 'int');
fwrite(fid, detector.opts.modelDsPad(1, 2), 'int');
fwrite(fid, detector.opts.stride, 'int');
fwrite(fid, detector.opts.cascThr, 'single');
fwrite(fid, 10, 'int'); %% number of channels
fwrite(fid, detector.opts.pPyramid.pChns.pColor.smooth, 'int');
fwrite(fid, detector.opts.pPyramid.pChns.pGradMag.normRad, 'int');
fwrite(fid, detector.opts.pPyramid.pChns.pGradMag.normConst, 'single');
fwrite(fid, detector.opts.pPyramid.pChns.pGradHist.nOrients, 'int');
temp = int32(detector.opts.pPyramid.pad / detector.opts.pPyramid.pChns.shrink);
fwrite(fid, temp(1, 1), 'int'); %% pad width after shrinking
fwrite(fid, detector.opts.pPyramid.lambdas(1, 2), 'single');
fwrite(fid, detector.opts.pNms.overlap, 'single');
%% cascade parameters
fwrite(fid, len, 'int');
fwrite(fid, td, 'int');
fwrite(fid, kol, 'int');
%% cascade trees
fwrite(fid, d.fids(1:4, 1:end), 'int');
fwrite(fid, d.thrs(1:4, 1:end), 'single');
fwrite(fid, d.hs(4:7, 1:end), 'single');
{mapping_write_code}
fclose(fid);
quit;
"""
mapping_write_code = """\
%% mappings
fwrite(fid, {lines_count}, 'int');
fwrite(fid, [{min_confidence:0.10f}, {max_confidence:0.10f}], 'single');
fwrite(fid, [{min_precision:0.10f}, {max_precision:0.10f}], 'single');
"""
def matlab_write_line(x_0, x_1, k, b):
    global mapping_write_code
    line = 'fwrite(fid, [%.10f, %.10f, %.10f], \'single\');\n' % (x_1, k, b)
    mapping_write_code += line

if no_mapping:
    mapping_write_code = '%% no mapping'
    def matlab_write_line(*args):
        pass

# mapping calculation here
if not no_mapping:
    data = open(roc_file, 'r').readlines()
    data = filter(lambda l: l != '', map(lambda l: l.split('#')[0].strip(), data))
    data = map(lambda l: map(float, l.split()), data)
    data.reverse()
    # at this point: data is a list of tuples (recall, precision, confidence), confidence ascending

    recall, precision, confidence = zip(*data)
    del data
    # converting precision from percents to ratio
    precision = map(lambda x: x / 100., precision)
    # backing up values
    if visualize_file:
        orig_confidence = confidence[:]
        orig_precision = precision[:]
    # fixing up data points
    fixing_methods_table[fixup_method](confidence, precision)

    # fitting piecewise-linear approximation
    lines = []
    begin_idx = 0
    max_idx = len(confidence) - 1
    # last_idx is not inclusive: [begin_idx, end_idx)
    while True:
        end_idx = begin_idx
        error = 0.
        poly = None
        while error < max_error and end_idx <= max_idx:
            end_idx += 1
            x_points = confidence[begin_idx : end_idx]
            y_points = precision[begin_idx : end_idx]
            poly = polyfit(x_points, y_points, 1)
            error = mean_squared_error(poly, x_points, y_points)
        # got polynom poly
        start = confidence[begin_idx]
        end = confidence[end_idx - 1]
        k, b = poly
        lines.append( (start, end, k, b) )
        # preparing next iteration
        begin_idx = end_idx - 1
        if begin_idx >= max_idx:
            break
    lines_count = len(lines)
    if lines_count == 0:
        print 'zero lines on output, stopping'
        exit(1)
    all_precisions = [] # to calculate max and min
    for line in lines:
        matlab_write_line(*line)
        k, b = line[2:4]
        all_precisions += map(lambda x: k * x + b, line[0:2])
    min_confidence = min(confidence)
    max_confidence = max(confidence)
    min_precision = min(all_precisions)
    max_precision = max(all_precisions)
    del all_precisions
    mapping_write_code = mapping_write_code.format(**locals())

# preparing matlab code and exec it
matlab_code = matlab_code.format(**locals())
tempdir = mkdtemp()
matscript = 'build_binary.m'
open(join(tempdir, matscript), 'w').write(matlab_code)
command = ['matlab', '-nodesktop', '-nodisplay', '-r', matscript[:-2]]
try:
    retcode = Popen(command, cwd = tempdir).wait()
except BaseException as err:
    print 'Working directory is %s' % tempdir
    print 'Matlab startup failed:'
    print '  ', err
    exit(1)
if retcode != 0:
    print 'Working directory is %s' % tempdir
    print 'Matlab returned error code %d' % retcode
    exit(1)
rmtree(tempdir)

if visualize_file:
    # setting good font (preferably, narrow)
    # it's suggested that confidence is sorted (ascending)
    range_confidence = max_confidence - min_confidence
    font = FontProperties(family = 'PT Sans Narrow', size = 14)
    figure(figsize = (20, 20), dpi = 101)
    axes = subplot()
    axes.set_xlabel('Confidence', fontproperties = font)
    axes.set_ylabel('Precision', fontproperties = font)
    axes.set_xlim([min_confidence - 0.01 * range_confidence, \
                   max_confidence + 0.01 * range_confidence])
    axes.set_ylim([-0.01, 1.01])
    axes.set_yticks(arange(0.0, 1.01, 0.05), minor = False)
    axes.set_yticks(arange(0.0, 1.01, 0.01), minor = True)
    axes.xaxis.set_major_locator(MaxNLocator(20))
    axes.xaxis.set_minor_locator(MaxNLocator(100))
    axes.grid(which = 'major', alpha = 0.9)
    axes.grid(which = 'minor', alpha = 0.2)
    plot(confidence, precision, '.-', aa = True, alpha = 0.7, label = 'Fixed curve')
    plot(orig_confidence, orig_precision, '.-', aa = True, alpha = 0.7, label = 'Original curve')
    for x_0, x_1, k, b in lines:
        y_vals = map(lambda x: k * x + b, [x_0, x_1])
        plot([x_0, x_1], y_vals, 'D-r', aa = True, alpha = 0.35, markersize = 10)
    map(lambda l: l.set_fontproperties(font), axes.get_xticklabels() + axes.get_yticklabels())
    legend(loc = 'lower right', prop = font)
    savefig(visualize_file, bbox_inches = 'tight')
    close()
print 'Done'
