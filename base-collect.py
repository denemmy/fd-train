#!/usr/bin/python2

from glob import iglob
from os import symlink, access, F_OK, mkdir
from argparse import ArgumentParser
from os.path import join, split, splitext, relpath, exists, basename
from sys import exit
from operator import sub

# NOTE: be aware, script DOES NOT honor exclude list affinity to some base:
#   if multiple bases have same-name element and it is excluded from one base,
#   it will be excluded from both bases

parser = ArgumentParser(description = 'Generate image base from other ones')
parser.add_argument('-s', '--src', type = str, nargs = '+', required = True, \
                    help = 'list of source directories')
parser.add_argument('-d', '--dst', type = str, required = True, help = \
                    'destination folder for database')
parser.add_argument('-m', '--max', type = int, default = None, help = \
                    'max count of images in database')
parser.add_argument('-e', '--exclude', nargs = '+', default = [], help = \
                    'exclude list of *images*')
parser.add_argument('--min-x-size', type = int, help = \
                    'minimal allowed x size of image (inclusive)')
parser.add_argument('--min-y-size', type = int, help = \
                    'minimal allowed y size of image (inclusive)')
arg = parser.parse_args()
sources, dst, exclude, count = arg.src, arg.dst, arg.exclude, arg.max
min_x, min_y = arg.min_x_size, arg.min_y_size

if min_x or min_y:
    from cv2 import imread
    def check_sizes(image_filename):
        try:
            img = imread(image_filename)
            y_size, x_size = img.shape[0:2]
            if min_x and x_size < min_x:
                return False
            if min_y and y_size < min_y:
                return False
        except:
            return False
        return True
else:
    def check_sizes(image_filename):
        return True

if exists(dst):
    print 'destination is already exists'
    exit(1)
dst_img_dir = join(dst, 'imgs')
dst_gt_dir = join(dst, 'gt')
map(mkdir, [dst, dst_img_dir, dst_gt_dir])

if count and count <= 0:
    print 'bad maximum size of set'
    exit(1)

# checking source database
for src in sources:
    src_img = join(src, 'imgs')
    src_gt = join(src, 'gt')
    if not all(map(lambda d: access(d, F_OK), [src, src_img, src_gt])):
        print 'check source directory, file tree format violated'
        exit(1)
# loading exclude list
if not all(map(lambda e: exists(e), exclude)):
    print 'exclude list accessing failure'
    exit(1)
# is a list of excluded image names
exclude = map(lambda f: map(str.strip, open(f).readlines()), exclude)
exclude = reduce(list.__add__, exclude, [])
exclude = set(exclude)

current_idx = 0
sample_format = 'merge%08d'
stop = False
for src in sources:
    src_img = join(src, 'imgs')
    src_gt = join(src, 'gt')

    for image_path in iglob('%s/*' % src_img):
        if not check_sizes(image_path):
            continue
        image = basename(image_path)
        if image in exclude:
            continue
        sample, image_ext = splitext(image)
        gt = sample + '.txt'
        gt_path = join(src_gt, gt)
        if not exists(gt_path):
            print 'gt accessing failure: %s' % gt_path
            continue
        # linking sample to new base
        dst_sample = sample_format % current_idx
        dst_gt = dst_sample + '.txt'
        dst_img = dst_sample + image_ext
        dst_gt_path = join(dst_gt_dir, dst_gt)
        dst_img_path = join(dst_img_dir, dst_img)
        # symlinking gt
        symlink(relpath(gt_path, dst_gt_dir), dst_gt_path)
        # symlinking image
        symlink(relpath(image_path, dst_img_dir), dst_img_path)
        # moving next
        current_idx += 1
        if count and current_idx >= count:
            stop = True
            break
    if stop:
        break
print 'processed %d files, done.' % current_idx
