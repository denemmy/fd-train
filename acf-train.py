#!/usr/bin/python2
# coding: utf8

from argparse import ArgumentParser
from os.path import isdir, isfile, join, exists, abspath
from os import access, R_OK, W_OK, X_OK, makedirs
from subprocess import Popen, PIPE, STDOUT
from sys import stdout
from datetime import datetime
from re import sub
from re import compile as regex_compile
from re import findall as regex_findall

parser = ArgumentParser(description = 'Train ACF detector')
add_arg = parser.add_argument
add_arg('--toolbox-dir', type = str, required = True, \
        help = 'path to Dollar\'s toolbox root')
add_arg('--input-base', type = str, required = True, \
        help = 'path to directory with sample images and gt')
add_arg('--model-size', type = str, required = True, \
        help = 'HxW string: sizes of unified sample for detection')
add_arg('--model-pad', type = str, required = True, \
        help = 'HxW string: sizes of padded unified sample for detection')
add_arg('--nweak', type = str, required = True, \
        help = 'A,B,C,...,D string: count of weak classifiers on each stage')
add_arg('--neg-count', type = int, required = True, \
        help = 'max number of negative samples to extract on one stage')
add_arg('--neg-per-image', type = int, required = True, \
        help = 'max number of negative samples to extract from one image')
add_arg('--max-neg', type = int, required = True, \
        help = 'max number of negatives to keep across stages')
add_arg('--nms-type', type = str, required = True, \
        help = 'non-maximum suppression type to be used')
add_arg('--nms-overlap', type = float, required = True, \
        help = 'non-maximum suppression overlap threshold')
add_arg('--label', type = str, required = True, \
        help = 'name for produces detector to find it in work directory')
add_arg('--workdir', type = str, required = True, \
        help = 'path to directory where work files and detectors are stored')
add_arg('--pool', action = 'store_true', \
        help = 'use or not matlab pool for parallel computing')
add_arg('--drop-x11', action = 'store_true', \
        help = 'add nodesktop and nodisplay options to matlab')
add_arg('--drop-jvm', action = 'store_true', \
        help = 'add nojvm option to matlab')
arg = parser.parse_args()

# sorting arguments and checking arguments
is_accessable_dir = lambda name: isdir(name) and access(name, R_OK | X_OK)
is_readable_file = lambda name: isfile(name) and access(name, R_OK)

toolbox = abspath(arg.toolbox_dir)
if not is_accessable_dir(toolbox):
    print 'toolbox directory accessing failure'
    exit(1)
version_path = join(toolbox, 'VERSION')
url_path = join(toolbox, 'URL')
if not is_readable_file(version_path):
    print 'version file is not found in toolbox directory'
    exit(1)
version = open(version_path).read().strip()
if not is_readable_file(url_path):
    print 'url file in not found in toolbox directory'
    exit(1)
url = open(url_path).read().strip()

base_dir = abspath(arg.input_base)
image_dir = join(base_dir, 'imgs')
gt_dir = join(base_dir, 'gt')
if not all(map(is_accessable_dir, [base_dir, image_dir, gt_dir])):
    print 'input image database accessing failure'
    exit(1)

try:
    model_size = map(int, arg.model_size.split('x'))[0:2]
except:
    print 'model-size parsing failure'
    exit(1)

try:
    pad_size = map(int, arg.model_pad.split('x'))[0:2]
except:
    print 'model-pad parsing failure'
    exit(1)

try:
    nweak = map(int, arg.nweak.split(','))
except:
    print 'nweak parsing failure'
    exit(1)

neg_per_stage = arg.neg_count
if neg_per_stage <= 0:
    print 'neg-count must be positive'
    exit(1)

neg_per_image = arg.neg_per_image
if neg_per_image <= 0:
    print 'neg-per-image must be positive'
    exit(1)

neg_overall = arg.max_neg
if neg_overall <= 0:
    print 'max-neg must be positive'
    exit(1)

nms_type = arg.nms_type
if nms_type not in {'max', 'maxg', 'ms', 'cover', 'none'}:
    print 'unsupported nms type'
    exit(1)

nms_value = arg.nms_overlap
if nms_value < 0. or nms_value > 1.:
    print 'nms-overlap is expected to be in [0, 1]'
    exit(1)

workdir = abspath(arg.workdir)
if exists(workdir):
    print 'workdir is already exists'
    exit(1)
makedirs(workdir)

detector_label = arg.label
use_pool = arg.pool
drop_x11 = arg.drop_x11
drop_jvm = arg.drop_jvm

if use_pool and drop_jvm:
    print 'matlab pool needs jvm enabled'
    exit(1)

#add double slashes
toolbox_db_sl = toolbox.replace("\\", "\\\\")
image_dir_db_sl = image_dir.replace("\\", "\\\\")
gt_dir_db_sl = gt_dir.replace("\\", "\\\\")
workdir_db_sl = workdir.replace("\\", "\\\\")

# matlab script template here
matlab_pool_line = 'matlabpool open' if use_pool else '% pool is disabled'
matlab_code = """\
diary('matlab.log');
try
    {matlab_pool_line}
    addpath(genpath('{toolbox_db_sl}'));
    options = acfTrain();
    options.posImgDir = '{image_dir_db_sl}\\';
    options.posGtDir = '{gt_dir_db_sl}\\';
    options.name = '{workdir_db_sl}\\';
    options.modelDs = {model_size};
    options.modelDsPad = {pad_size};
    options.nWeak = {nweak};
    options.nNeg = {neg_per_stage};
    options.nPerNeg = {neg_per_image};
    options.nAccNeg = {neg_overall};
    options.pNms = {{ 'type', '{nms_type}', 'overlap', {nms_value} }};
    detector = acfTrain(options);
catch error
    fprintf(getReport(error, 'extended'));
    exit(1);
end
diary off;
exit(0);
"""
# instantiation of matlab code template
matlab_code = matlab_code.format(**locals())
matlab_func = 'exec_training'
matlab_file = '%s.m' % matlab_func
open(join(workdir, matlab_file), 'w').write(matlab_code)

# preparing matlab command
command = ['matlab', '-nosplash', '-wait']
if drop_x11:
    command += ['-nodesktop', '-nodisplay']
if drop_jvm:
    command += ['-nojvm']
command += ['-r', matlab_func]
proc = Popen(command, cwd = workdir,
             stdout=PIPE,
             stderr=STDOUT)
retcode = proc.wait()

# writing commentaries about the detector
comment_base = """\
the report is built at: {current_time}
Dollar toolbox version used: {version}
detector label is: {detector_label}

input image database: {base_dir}
target model sizes (WxH): {model_size[1]}x{model_size[0]}
target model padding: {pad_size[1]}x{pad_size[0]}
weak classifiers per stage: {nweak}
negative per stage: {neg_per_stage}
negative per image: {neg_per_image}
overall negatives: {neg_overall}
non-maximum suppression type: {nms_type}
non-maximum suppression threshold: {nms_value}

matlab return code: {retcode}
time on training elapsed: {consumed_time}

matlab invokation log:
{matlab_log}
"""
current_time = str(datetime.now())
consumed_time = 'N/A'

matlab_log_filename = join(workdir, 'matlab.log')
log_lines = open(matlab_log_filename, 'r').readlines()[7:]
# removing progress bars with backspace special symbol
remove_spinner_regex = regex_compile('^.*completed.*\x08')
log_lines = map(lambda s: remove_spinner_regex.sub('', s), log_lines)
# searching for last timer measurement printout
fulltime_regex = regex_compile('^Done training \(time=(.*)\)\.$')
target = filter(lambda l: len(l) == 1, map(lambda s: regex_findall(fulltime_regex, s), log_lines))
if len(target) == 1:
    consumed_time = target[0][0]
# preparing full matlab invokation log
matlab_log = ''.join(log_lines)
# writing comments to file
open(join(workdir, 'comment.txt'), 'w').write(comment_base.format(**locals()))

# exiting
if retcode != 0:
    print '# return code indicates error: %d' % retcode
    exit(1)
exit(0)
