#!/usr/bin/python2
# coding: utf8

from os.path import basename, splitext, exists
from os import mkdir
from operator import itemgetter
from glob import iglob, glob
from argparse import ArgumentParser
import cv2

parser = ArgumentParser(description = 'Draw detections and gt bounding boxes over images')
parser.add_argument('-b', '--imagebase-dir', type = str, required = True, help = \
                    'data folder with images and gt')
parser.add_argument('-d', '--detections-file', type = str, required = True, help = \
                    'text file with detections: "file;x;y;w;h;confidence"')
parser.add_argument('-l', '--iou-level', type = float, default = 0.5, help = \
                    'acceptance level for intersection over union thresh')
parser.add_argument('-o', '--output-dir', type = str, required = True, help = \
                    'directory for output images')
parser.add_argument('-c', '--min-confidence', type = float, default = 0, help = \
                    'minimal accepted confidence')
parser.add_argument('-g', '--draw-gt', action = 'store_true', help = \
                    'select if you want to see gt bboxes')

arg = parser.parse_args()
TARGET = arg.imagebase_dir
OUTPUT = arg.detections_file
IOU_THRESH = arg.iou_level
OUT_PATH = arg.output_dir
CONF = arg.min_confidence
GT = arg.draw_gt

def overlap(l, r):
    # format: l = [point, length]
    # returns overlap for 1d segments
    if l[0] > r[0]:
        l, r = r, l
    far_r, far_l = map(sum, (r, l))
    if r[0] > far_l:
        return 0
    if far_l > far_r:
        return r[1]
    return far_l - r[0]

def IoU(a, b):
    # format: a = [x, y, w, h], same for b
    # returns overlap ratio: IoverU
    x_overlap = overlap((a[0], a[2]), (b[0], b[2]))
    y_overlap = overlap((a[1], a[3]), (b[1], b[3]))
    common = 1. * x_overlap * y_overlap
    union = a[2] * a[3] + b[2] * b[3] - common
    return common / union

def get_basename(filename):
    return splitext(basename(filename))[0]

def box(rects, img, color):
    for x, y, w, h in rects:
            cv2.rectangle(img, (x, y), (x + w, y + h), color, 2, cv2.CV_AA)

def textbox(image, point, text):
    bbox, ign = cv2.getTextSize(text, cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, 1)
    x, y = point
    w, h = bbox
    d = 2
    cv2.rectangle(image, (x - d, y - d), (x + w + d, y + h + d), (0, 255, 255), cv2.cv.CV_FILLED, cv2.CV_AA)
    cv2.putText(image, text, (x, y + h), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 0, 0), 1, cv2.CV_AA)

# output file format is 'filename;min-x;min-y;width;height;confidence'
# target folder contain 'imgs' and 'gt' with same file names.
# 'gt' format is 'label x y w h ...'

print '# reading detection file'
detections = open(OUTPUT).readlines()
detections = map(lambda x: x.strip().split(';'), detections)
det_dict = {} # basename -> [[x y w h conf]]

print '# passing over detection entries'
for elem in detections:
    base_name = get_basename(elem[0])
    detection = map(int, elem[1:5]) + [float(elem[5])]
    det_dict[base_name] = det_dict.get(base_name, []) + [detection]

# loading gt information
if not exists(OUT_PATH):
    mkdir(OUT_PATH)

print '# reading gt files'
for image_name in glob(TARGET + '/imgs/*.*'):
    base_name = get_basename(image_name)
    # gt bboxes
    gt_file = TARGET + '/gt/' + base_name + '.txt'
    if not exists(gt_file):
        gt_bboxes = []
    else:
        gt_lines = open(gt_file).readlines()
        gt_bboxes = map(lambda l: map(int, l.split()[1:5]), gt_lines)
    # detections bboxes
    dets = det_dict.get(base_name, [])
    # building summary
    good = []
    bad = []
    iou_labels = [] # (x, y, text)
    for detection in dets:
        det_bbox = detection[0:4]
        conf = detection[4]
        if conf < CONF:
            continue
        best_match = max(map(lambda bbox: IoU(bbox, det_bbox), gt_bboxes))
        iou_labels.append((det_bbox[0], det_bbox[1] + det_bbox[3] + 12, 'IOU %.0f' % (100 * best_match)))
        if best_match > IOU_THRESH:
            good.append(detection)
        else:
            bad.append(detection)
    image = cv2.imread(image_name)
    if GT:
        box(gt_bboxes, image, (0, 255, 255))
    box(map(lambda l: l[0:4], good), image, (0, 255, 0))
    box(map(lambda l: l[0:4], bad), image, (0, 0, 255))
    for x, y, txt in iou_labels:
        textbox(image, (x, y), txt)

    if len(good + bad) == 0:
        textbox(image, (10, 30), 'No detections found')
    for detection in good + bad:
        x, y, w, h = detection[0:4]
        conf = detection[4]
        textbox(image, (x, y + h + 29), 'LVL %.0f' % conf)
    cv2.imwrite(OUT_PATH + '/' + base_name + '.jpg', image)
